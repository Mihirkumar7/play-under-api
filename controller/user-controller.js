const UserModel = require('../model/user-model.js');
const userService = require('../service/user-service.js')

async function createUsers(req, res) {
    try {
        const validationRes = await userService.validateUsers(req.body);
        await UserModel.insertMany(validationRes.validUsers);
        res.status(200).send(validationRes.responseObject);
    } catch (err) {
        res.status(400).send(err)
    }
}

async function findUsers(req, res){
    try {
        const users = await UserModel.find();
        res.status(200).send(users)
    } catch (error) {
        res.status(400).send(err)
    }
}

module.exports = {createUsers, findUsers}