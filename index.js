const express = require('express')
require('./db/mongoose.js')
const router = require('./routes/router.js');
const PORT = 3000
const app = express()

app.use(express.json())
app.use(router)

app.listen(PORT, () => {
    console.log(`Listening on PORT ${PORT}.....`)
})