var fs = require('fs');
const { format } = require('@fast-csv/format');

async function validateUsers(users) {
    const validUsers = users.filter(x => x.name.length <= 50 && x.age >= 20 && x.age <= 80);
    const invalidUsers = users.filter(x => x.name.length > 50 || x.age < 20 || x.age > 80);
    createCsv(validUsers, 'store_csv/success.csv');
    createCsv(invalidUsers, 'store_csv/failed.csv');
    const responseObject = await createResponseObject(validUsers, invalidUsers);
    return { validUsers, responseObject}
}

// async function validateUsersUsingRecursion(users){
//     const validUsers = [];
//     const invalidUsers = [];


 function createCsv(users, fileName) {
    const file = fs.createWriteStream(fileName);
    const stream = format();
    stream.pipe(file);
    for (let i = 0; i < users.length; i++) {
        stream.write(users[i]);
    }
    stream.end();
}


 function createResponseObject(validUsers, invalidUsers) {
    let response = {
        "success": {
            "count": validUsers.length,
            "data": validUsers
        },
        "failed": {
            "count": invalidUsers.length,
            "data": invalidUsers
        }
    }
    return response;
}

exports.validateUsers = validateUsers ;